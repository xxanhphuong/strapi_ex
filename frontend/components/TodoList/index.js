import React, { useState, useEffect } from 'react';
import gql from "graphql-tag";
import Link from "next/link";
import { graphql } from "react-apollo";
import axios from 'axios';
import { useForm } from "react-hook-form";
import ReactHookForm from '../ReactHookForm';

import {
    Button,
    Card,
    CardBody,
    CardColumns,
    CardImg,
    CardSubtitle
} from "reactstrap";
import { CardText, CardTitle, Col, Row } from "reactstrap";
const TodoList = ({ data: { loading, error, todos } }) => {

    // if (todos && todos.length) {
    const [todoList, setTodoList] = useState(todos);
    const [data, setData] = useState({
        decriptions: "",
    });

    useEffect(() => {
        setTodoList(todos);
    }, [todos])



    const addTodo = (todo) => {
        console.log(todo)
        axios.post('http://localhost:1337/todos', {
            task: todo.task,
            decriptions: todo.descriptions,
        })
            .then(function (response) {
                const newTodos = [response.data, ...todoList];
                setTodoList(newTodos);
                console.log(newTodos);
            })
            .catch(function (error) {
                console.log(error);
            });
    };
    const editTodo = (id) => {
        console.log(id, data);
        axios.put('http://localhost:1337/todos/' + id, data)
            .then(function (response) {
                const newTodoList = todoList.filter((todo) => {
                    if (todo.id == response.data.id) {
                        todo.decriptions = response.data.decriptions;
                    }

                    return todo;
                })
                setTodoList(newTodoList);
                console.log('delete', newTodoList)
            })
            .catch(function (error) {
                console.log(error);
            });
    };
    const deleteTodo = (id) => {
        axios.delete('http://localhost:1337/todos/' + id)
            .then(function (response) {
                const newTodoList = todoList.filter((todo) => {

                    return (todo.id != response.data.id);
                })
                setTodoList(newTodoList);
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    const onChange = (propertyName, event) => {
        data[propertyName] = event.target.value;
        console.log()
        setData(data);
    };
    return (
        <Card
            style={{ width: "100%", margin: "20px 10px" }}
        >
            <ReactHookForm addTodo={addTodo} />

            <CardBody>
                {todoList ? todoList.map((i, key) => (
                    <div style={{ margin: "20px 10px", padding: "20px 10px", width: '100%' }} key={key} className="d-flex">
                        {/* <CardImg style={{ margin: "20px 10px", padding: "20px 10px", width: '100%' }} src={`http://localhost:1337${i.image[0].url}`} alt="Card image cap" /> */}
                        <CardBody style={{ background: '#f7f7f7' }}>
                            <CardTitle>{i.task}</CardTitle>
                            <div className="d-flex align-items-center">
                                <input style={{ border: 'none', padding: '8px', marginRight: '8px' }} defaultValue={i.decriptions} onChange={onChange.bind(this, "decriptions")}></input>
                                <Button onClick={() => deleteTodo(i.id)}>Delete</Button>
                                <Button onClick={() => editTodo(i.id)} className="ml-4">Edit</Button>
                            </div>
                        </CardBody>
                    </div>))
                    : ''}
            </CardBody>
        </Card>
    )
    // }
};

const query = gql`
{
    todos {
      id
      task
      image{
        url
      }
      decriptions
      isDone
    }
  }
`;
export default graphql(query, {
    props: ({ data }) => ({
        data
    })
})(TodoList);