import React, { Component } from "react";
import { useForm } from "react-hook-form";

const ReactHookForm = (props) => {
    const { handleSubmit, register, errors } = useForm();
    const onSubmit = values => props.addTodo(values);
    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Task</label>
            <input
                name="task"
                ref={register({
                    required: "Required",
                })}
            />
            <label>descriptions</label>
            <input
                name="descriptions"
                ref={register({
                    validate: value => value !== "admin" || "Nice try!"
                })}
            />
            {errors.username && errors.username.message}

            <button type="submit">Submit</button>
        </form>
    );
};

export default ReactHookForm;